export const coursesFilter = (items, searchQuery) => {
	return items.filter(
		(course) =>
			course.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
			course.id.toLowerCase().includes(searchQuery)
	);
};

export const pipeDurationTime = (MINUTES) => {
	let m = MINUTES % 60;

	let h = (MINUTES - m) / 60;

	return (
		(h < 10 ? '0' : '') +
		h.toString() +
		':' +
		(m < 10 ? '0' : '') +
		m.toString() +
		' hours'
	);
};

export const pipeDurationDate = (DATE) => {
	return DATE.replaceAll('/', '.');
};

import axios from 'axios';

export const API_URL = `http://localhost:4000/`;

const $host = axios.create({
	baseURL: API_URL,
});

const $authHost = axios.create({
	baseURL: API_URL,
});

const authInterceptor = (config) => {
	config.headers.Authorization = localStorage.getItem('token');
	return config;
};

$authHost.interceptors.request.use(authInterceptor);

export { $host, $authHost };

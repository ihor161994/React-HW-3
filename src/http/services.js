import { $authHost, $host } from './index';

export const registration = async (newUser) => {
	const { data } = await $host.post('/register', newUser);
	return data;
};

export const login = async (user) => {
	const { data } = await $host.post('/login', user);
	/*localStorage.setItem('token', data.result);*/
	return data;
};

export const logout = async () => {
	const response = await $authHost.delete('/logout');
	/*localStorage.removeItem('token');*/
	return response;
};

export const getAllCourses = async () => {
	const { data } = await $authHost.get('/courses/all');
	return data;
};

export const getAllAuthors = async () => {
	const { data } = await $authHost.get('/authors/all');
	return data;
};

export const getUsersMe = async () => {
	const { data } = await $authHost.get('/users/me');
	return data;
};

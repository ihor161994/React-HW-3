import React from 'react';
import styles from './Button.module.scss';
export const Button = ({ text, onClick, ...props }) => (
	<button {...props} className={styles.button} onClick={onClick}>
		{text}
	</button>
);

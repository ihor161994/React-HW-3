import { LOGIN_USER, LOGOUT_USER } from './actionTypes';

export const loginUserData = (payload) => ({ type: LOGIN_USER, payload });
export const logoutUserData = (payload) => ({ type: LOGOUT_USER, payload });

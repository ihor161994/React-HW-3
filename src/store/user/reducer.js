import { LOGIN_USER, LOGOUT_USER } from './actionTypes';

const userInitialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
};

export const userReducer = (state = userInitialState, action) => {
	switch (action.type) {
		case LOGIN_USER: {
			localStorage.setItem('token', action.payload.token);
			return { ...action.payload };
		}
		case LOGOUT_USER: {
			localStorage.removeItem('token');
			return { ...userInitialState };
		}
		default:
			return state;
	}
};

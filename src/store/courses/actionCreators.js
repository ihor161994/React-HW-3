import {
	ADD_COURSES,
	CREATE_COURSE,
	DELETE_COURSE,
	UPDATE_COURSE,
} from './actionTypes';

export const addCoursesData = (payload) => ({ type: ADD_COURSES, payload });
export const deleteCourseData = (payload) => ({ type: DELETE_COURSE, payload });
export const createCourseData = (payload) => ({ type: CREATE_COURSE, payload });
export const updateCourseData = (payload) => ({ type: UPDATE_COURSE, payload });

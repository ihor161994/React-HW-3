import {
	ADD_COURSES,
	CREATE_COURSE,
	DELETE_COURSE,
	UPDATE_COURSE,
} from './actionTypes';

const coursesInitialState = [];

export const coursesReducer = (state = coursesInitialState, action) => {
	switch (action.type) {
		case ADD_COURSES:
			return [...action.payload];
		case DELETE_COURSE:
			return [...state.filter((course) => course.id !== action.payload)];
		case CREATE_COURSE:
			return [...state, action.payload];
		case UPDATE_COURSE:
			return [
				...state,
				state.map((course) =>
					course.id === action.payload.id ? action.payload : course
				),
			];
		default:
			return state;
	}
};

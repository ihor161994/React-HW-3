import { ADD_AUTHORS, CREATE_AUTHOR } from './actionTypes';

export const addAuthorsData = (payload) => ({ type: ADD_AUTHORS, payload });
export const createAuthorData = (payload) => ({ type: CREATE_AUTHOR, payload });

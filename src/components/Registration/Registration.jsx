import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { Button } from '../../common/Button/Button';
import Input from '../../common/Input/Input';

import styles from './Registration.module.scss';
import { registration } from '../../http/services';

const Registration = () => {
	const navigate = useNavigate();

	const [newUser, setNewUser] = useState({
		name: '',
		password: '',
		email: '',
	});

	const signUp = async (e) => {
		e.preventDefault();
		try {
			const response = await registration(newUser);
			response.successful ? navigate('/') : alert(response.errors);
		} catch (e) {
			alert(e.response.data.errors);
		}
	};

	return (
		<div className={'container'}>
			<div className={styles.registration}>
				<h3>Registration</h3>
				<form onSubmit={(e) => signUp(e)}>
					<Input
						placeholderText={'Enter name'}
						labelText={'Name'}
						onChange={(e) => setNewUser({ ...newUser, name: e.target.value })}
						value={newUser.name}
					/>
					<Input
						placeholderText={'Enter email'}
						labelText={'Email'}
						onChange={(e) => setNewUser({ ...newUser, email: e.target.value })}
						value={newUser.email}
						type={'email'}
					/>
					<Input
						placeholderText={'Enter password'}
						labelText={'Password'}
						onChange={(e) =>
							setNewUser({ ...newUser, password: e.target.value })
						}
						value={newUser.password}
						type={'password'}
					/>
					<Button text={'Registration'} />
				</form>
				<p>
					If you have an account you can{' '}
					<Link to='/login' className={styles.login}>
						Login
					</Link>
				</p>
			</div>
		</div>
	);
};

export default Registration;

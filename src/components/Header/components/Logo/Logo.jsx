import React from 'react';
import logo from '../../../../images/logo192.png';
import styles from './Logo.module.scss';

export const Logo = () => (
	<img className={styles.logo} src={logo} alt={'logo'} />
);

import React from 'react';

import { Logo } from './components/Logo/Logo';
import { Button } from '../../common/Button/Button';

import styles from './Header.module.scss';
import { useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logoutUserData } from '../../store/user/actionCreators';
import { logout } from '../../http/services';

const Header = () => {
	const location = useLocation().pathname;
	const dispatch = useDispatch();
	const user = useSelector((state) => state.user);

	const isTools = () => {
		return location === '/login' || location === '/registration';
	};

	const navigate = useNavigate();

	const logOut = async () => {
		try {
			await logout();
			dispatch(logoutUserData());
			navigate('/login');
		} catch (e) {
			alert(e.response.data.result);
		}
	};

	const handleLogin = () => {
		navigate('/login');
	};

	return (
		<div className='container'>
			<div className={styles.header}>
				<Logo />
				{!isTools() && (
					<div className={styles.tools}>
						{!user.isAuth ? (
							<Button text={'Login'} onClick={handleLogin} />
						) : (
							<>
								<div className={styles.userName}>{user.name}</div>
								<Button text={'Logout'} onClick={logOut} />
							</>
						)}
					</div>
				)}
			</div>
		</div>
	);
};

export default Header;

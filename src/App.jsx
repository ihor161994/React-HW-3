import Header from './components/Header/Header';
import './App.css';
import Courses from './components/Courses/Courses';
import {
	BrowserRouter as Router,
	Navigate,
	Route,
	Routes,
} from 'react-router-dom';
import CreateCourse from './components/CreateCourse/CreateCourse';

import { useEffect } from 'react';

import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';

import { useDispatch, useSelector } from 'react-redux';
import { loginUserData } from './store/user/actionCreators';
import { addCoursesData } from './store/courses/actionCreators';
import { addAuthorsData } from './store/authors/actionCreators';
import { getUser } from './store/selectors';
import { getAllAuthors, getAllCourses, getUsersMe } from './http/services';

function App() {
	const dispatch = useDispatch();
	const user = useSelector(getUser);

	useEffect(() => {
		if (localStorage.getItem('token')) {
			async function getUser() {
				try {
					const response = await getUsersMe();
					const { name, email } = response.result;
					dispatch(
						loginUserData({
							name,
							email,
							token: localStorage.getItem('token'),
							isAuth: true,
						})
					);
				} catch (e) {
					console.log(e.response.statusText);
				}
			}

			getUser();
		}
	}, []);

	useEffect(() => {
		async function getCourses() {
			try {
				const response = await getAllCourses();
				dispatch(addCoursesData(response.result));
			} catch (e) {
				console.log(e.response.data.result);
			}
		}

		async function getAuthors() {
			try {
				const response = await getAllAuthors();
				dispatch(addAuthorsData(response.result));
			} catch (e) {
				console.log(e.response.data.result);
			}
		}

		getCourses();
		getAuthors();
	}, []);

	return (
		<div className='App'>
			<Router>
				<Header />

				<Routes>
					<Route exact path='/registration' element={<Registration />} />
					<Route exact path='/login' element={<Login />} />
					{/*{!user.isAuth ? (
						<Route path='*' element={<Navigate to={'/login'} />} />
					) : (
						<>
							<Route path='*' element={<Navigate to={'/courses'} />} />
							<Route exact path='/courses'>
								<Route index element={<Courses />} />
								<Route exact path=':courseId' element={<CourseInfo />} />
								<Route exact path='add' element={<CreateCourse />} />
							</Route>
						</>
					)}*/}

					<Route exact path='/courses'>
						<Route index element={<Courses />} />
						<Route exact path=':courseId' element={<CourseInfo />} />
						<Route exact path='add' element={<CreateCourse />} />
					</Route>
					<Route
						path='*'
						element={
							user.isAuth ? (
								<Navigate to={'/courses'} />
							) : (
								<Navigate to={'/login'} />
							)
						}
					/>
				</Routes>
			</Router>
		</div>
	);
}

export default App;
